package com.geodani.sda.quiz.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.geodani.sda.quiz.model.Question;

public interface QuestionRepository extends JpaRepository<Question, Long> {

}
