package com.geodani.sda.quiz.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.geodani.sda.quiz.model.Question;
import com.geodani.sda.quiz.service.QuestionService;

@RestController
@RequestMapping(value="/api/questions")
public class QuestionController {
	
	@Autowired
	private QuestionService questionService;
	
	//@CrossOrigin(origins = "http://localhost:9090")
	@GetMapping(value="/all")
	public ResponseEntity<List<Question>> getQuestions(){
		List<Question> questions = questionService.findAll();
		if(questions.isEmpty()){
	       return new ResponseEntity<List<Question>>(HttpStatus.NO_CONTENT);
	    }
		return new ResponseEntity<List<Question>>(questions, HttpStatus.OK);
	}

}
