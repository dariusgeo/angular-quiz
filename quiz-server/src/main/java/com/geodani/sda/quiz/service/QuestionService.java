package com.geodani.sda.quiz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.geodani.sda.quiz.model.Question;
import com.geodani.sda.quiz.respository.QuestionRepository;

@Service
public class QuestionService {
	
	@Autowired
	private QuestionRepository repository;
	
	public List<Question> findAll(){
		
		return repository.findAll();
	}
	
	//create
	// 1. void daca vreau creez entitatea si nu ma mai intereseaza
	//   'Nu ma mai intereseaza' == vad din alt modul/ vad din alta pagina
	// 2. long == entity id (REST) => ii dau UI-ului id-ul resursei nou create
	// 3. entity nou creata => dupa creare sunt redirectat pe detalii
	public void create(Question question) {
		repository.save(question); // se aloca id din baza de date
	}
	
	public Question update(Question question) {		
		return null;
	}
	
    public void delete(Question question) {
	}    

}
