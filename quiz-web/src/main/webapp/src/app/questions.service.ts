import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor(private http: HttpClient) { }

  getQuestions(){	
	return this.http.get('http://localhost:9090/api/questions/all')
	//return this.http.get('https://regres.in/api/users')
  }
}
