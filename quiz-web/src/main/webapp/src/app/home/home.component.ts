import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../questions.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  questions: Object;

  constructor(private questionsService: QuestionsService) { }

  ngOnInit() {
	this.questionsService.getQuestions().subscribe(data => {
		this.questions = data
		console.log(this.questions)
	})
  }
}
